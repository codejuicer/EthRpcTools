import 'mocha';
import {expect} from 'chai';
import {Account, Block} from "../src/client/index";
import {RpcClient} from "../src/net";

describe('Account tests', () => {
  let mockRpc: RpcClient;
  let mockBlock: Block;

  before(() => {
    mockRpc = new RpcClient();
  });

  after(() => {

  });

  it('Gets the account hash', () => {
    const acctHash = '0xabcdef012345';
    const account = new Account(acctHash, mockRpc);
    expect(account.account).to.equal(acctHash);
  });

  it('Changes the account hash', () => {
    const acctHash = '0xabcdef012345';
    const account = new Account('0x0', mockRpc);
    account.account = acctHash;
    expect(account.account).to.equal(acctHash);
  });

});