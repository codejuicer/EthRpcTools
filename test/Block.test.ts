import 'mocha';
import {expect} from 'chai';
import * as sinon from 'sinon';
import {Account, Block, RpcClient} from "../src/client/index";
import {TestBlocks} from "./res";

describe('Block tests', () => {
    let mockRpc: RpcClient;

    before(() => {
        mockRpc = new RpcClient();
    });

    after(() => {

    });

    it('Creates a new block', () => {
        const block = new Block(TestBlocks.Block0);
        expect(block.number).to.equal(0);
    });

    it('Gets the current block number', (done) => {
        let callStub = sinon.stub(mockRpc, "call").callsFake(() => {
           return Promise.resolve(TestBlocks.Block0);
        });

        callStub.restore();
    });

});