import 'mocha';
import {expect} from 'chai';
import {CoinMarketCap} from "../../src/market/CoinMarketCap";

describe('CoinMarketCap tests', () => {

  it('has a listings endpoint', () => {
    expect((new CoinMarketCap()).hasEndpoint('listings/')).to.be.true;
  });

  it('does not have a bunny endpoint', () => {
    expect((new CoinMarketCap()).hasEndpoint('bunny/')).to.be.false;
  });

  it('returns nothing for non-existing endpoint', () => {
    expect((new CoinMarketCap()).getEndpointDefinition('nothere/')).to.be.undefined;
  });

  it('returns a definition for an existing endpoint', () => {
    const api = new CoinMarketCap();
    expect(JSON.stringify(api.getEndpointDefinition('listings/')))
      .to.equal('{"endpoint":"listings/","restMethod":"GET"}');
  });
});