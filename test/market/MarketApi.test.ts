import 'mocha';
import {expect} from 'chai';
import {MarketApi} from "../../src/market/MarketApi";
import {CoinMarketCap} from "../../src/market/CoinMarketCap";

describe('Market API tests', () => {

  it('gets a listing', (done) => {
    // TODO: Create fake market cap to remove dependency on coinmarketcap.com
    const coinMarketCap = new CoinMarketCap();
    let marketApi = new MarketApi(new CoinMarketCap());

    marketApi.call()
      .withEndpoint(coinMarketCap.getEndpointDefinition('listings'))
      .invoke()
      .catch((err) => done(err))
      .then((data) => {
        expect(data).to.haveOwnProperty('data');
        done();
      });
  });
});