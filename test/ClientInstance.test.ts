import 'mocha';
import {expect} from 'chai';
import * as sinon from 'sinon';
import {RpcClient} from "../src/client/index";
import {ClientInstance} from "../src/client/ClientInstance";

describe('ClientInstance tests', () => {
  let mockRpc: RpcClient;

  before(() => {
    mockRpc = new RpcClient();
  });

  after(() => {

  });

  //---------------------------------------------------------------------------
  // Coin naming tests
  //---------------------------------------------------------------------------
  describe('Coin type', () => {
    it('Tests coin name default', () => {
      const client = new ClientInstance(mockRpc);
      expect(client.coinName).to.equal('Ethereum');
    });

    it('Tests coin abbreviation default', () => {
      const client = new ClientInstance(mockRpc);
      expect(client.coinAbbreviation).to.equal('ETH');
    });

    it('Tests changing the coin', () => {
      let client = new ClientInstance(mockRpc);
      const coinName = 'New coin';
      const coinAbbreviation = 'NC';

      client.coin = {coinName: coinName, coinAbbreviation: coinAbbreviation};
      expect(client.coinName).to.equal(coinName);
      expect(client.coinAbbreviation).to.equal(coinAbbreviation);
    });

    it('Tests the coin getter', () => {
      let coin = {coinName: 'Pickle', coinAbbreviation: 'PCK'};
      let client = new ClientInstance(mockRpc, coin);
      expect(client.coin).to.equal(coin);
    });
  });

  //---------------------------------------------------------------------------
  // RPC interactions
  //---------------------------------------------------------------------------
  describe('coinbase functions', () => {
    it('Tests getting the coinbase', (done) => {
      const expectedCoinbase = '0xdeadbeef';

      sinon.stub(mockRpc, 'call')
        .callsFake(() => {
          return Promise.resolve(expectedCoinbase);
        });

      let client = new ClientInstance(mockRpc);
      client.coinbase.then((coinbase) => {
        expect(coinbase).to.equal(expectedCoinbase);
        done();
      });

      mockRpc.call.restore();
    });

    it('Tests coinbase is undefined on rpc error.', (done) => {
      sinon.stub(mockRpc, 'call')
        .callsFake(() => {
          return Promise.reject(new Error('AAH! Wait... just a mock. This is good.'));
        });

      let client = new ClientInstance(mockRpc);
      client.coinbase.then((coinbase) => {
        expect(coinbase).to.be.undefined;
        done();
      });

      mockRpc.call.restore();
    });
  });

  describe('Account list functions', () => {
    it('Gets an account list', (done) => {
      const expectedAddressList = ['0x4e927abcdef782940a0f', '0x4e927abcdef782940a0f'];

      sinon.stub(mockRpc, 'call')
        .callsFake(() => {
          return Promise.resolve(expectedAddressList);
        });

      let client = new ClientInstance(mockRpc);
      client.accounts.then((addresses) => {
        expect(addresses.join(',')).to.equal(expectedAddressList.join(','));
        done();
      });

      mockRpc.call.restore();
    });

    it('Gets an empty account list', (done) => {
      const expectedAddressList = [];

      sinon.stub(mockRpc, 'call')
        .callsFake(() => {
          return Promise.resolve(expectedAddressList);
        });

      let client = new ClientInstance(mockRpc);
      client.accounts.then((addresses) => {
        expect(addresses.join(',')).to.equal(expectedAddressList.join(','));
        done();
      });

      mockRpc.call.restore();
    });

    it('Account list is undefined on RPC error', (done) => {
      sinon.stub(mockRpc, 'call')
        .callsFake(() => {
          return Promise.reject(new Error('Sorry, no accounts for you.'));
        });

      let client = new ClientInstance(mockRpc);
      client.accounts.then((addresses) => {
        expect(addresses).to.be.undefined;
        done();
      });

      mockRpc.call.restore();
    });
  });

  describe('Account testing', () => {
    it('Gets an account object', () => {
      const client = new ClientInstance(mockRpc);
      const expected = '0xNewAccount';

      expect(client.getAccount(expected).account).to.equal(expected);
    });
  })

});