import 'mocha';
import {expect} from 'chai';
import {ConsoleColors, StringUtility} from "../src/client/Utility";


describe('Utility tests', () => {
  it('colors some text red', () => {
    const expected = '\u001b[31mRED TEXT\u001b[0m';
    const actual = StringUtility.colorString({fg: ConsoleColors.RED}, "RED TEXT");
    console.log(actual);
    expect(actual).to.equal(expected);
  });
});