let Client = require('node-rest-client').Client;

export const RpcMethods = {
    eth: {
        accounts: 'eth_accounts',
        blockNumber: 'eth_blockNumber',
        call: 'eth_call',
        coinbase: 'eth_coinbase',
        estimateGas: 'eth_estimateGas',
        gasPrice: 'eth_gasPrice',
        getBalance: 'eth_getBalance',
        getBlockByHash: 'eth_getBlockByHash',
        getBlockByNumber: 'eth_getBlockByNumber',
        getCode: 'eth_getCode',
        getFilterChanges: 'eth_getFilterChanges',
        getFilterLogs: 'eth_getFilterLogs',
        getLogs: 'eth_getLogs',
        getStorageAt: 'eth_getStorageAt',
        getTransactionByBlockHashAndIndex: 'eth_getTransactionByBlockHashAndIndex',
        getTransactionByBlockNumberAndIndex: 'eth_getTransactionByBlockNumberAndIndex',
        getTransactionByHash: 'eth_getTransactionByHash',
        getTransactionCount: 'eth_getTransactionCount',
        getTransactionReceipt: 'eth_getTransactionReceipt',
        getUncleByBlockHashAndIndex: 'eth_getUncleByBlockHashAndIndex',
        getUncleByBlockNumberAndIndex: 'eth_getUncleByBlockNumberAndIndex',
        getUncleCountByBlockHash: 'eth_getUncleCountByBlockHash',
        getUncleCountByBlockNumber: 'eth_getUncleCountByBlockNumber',
        getWork: 'eth_getWork',
        hashrate: 'eth_hashrate',
        mining: 'eth_mining',
        newBlockFilter: 'eth_newBlockFilter',
        newFilter: 'eth_newFilter',
        newPendingTransactionFilter: 'eth_newPendingTransactionFilter',
        protocolVersion: 'eth_protocolVersion',
        sendRawTransaction: 'eth_sendRawTransaction',
        sendTransaction: 'eth_sendTransaction',
        sign: 'eth_sign',
        signTransaction: 'eth_signTransaction',
        submitHashrate: 'eth_submitHashrate',
        submitWork: 'eth_submitWork',
        syncing: 'eth_syncing',
        uninstallFilter: 'eth_uninstallFilter'
    },
    personal: {
        listAccounts: 'personal_listAccounts',
        newAccount: 'personal_newAccount',
        sendTransaction: 'personalSendTransaction',
        signTransaction: 'personal_signTransaction',
        unlockAccount: 'personal_unlockAccount',
        sign: 'personal_sign',
        ecRecover: 'personal_ecRecover'
    },
    net: {
        listening: 'net_listening',
        peerCount: 'net_peerCount',
        version: 'net_version'
    }
};

/**
 * A client for interacting with RPC commands
 */
export class RpcClient {
    private readonly _rpcAddress: string;
    private readonly _rpcPort: number;
    private readonly _protocol: string;

    constructor(rpcAddress = '127.0.0.1', rpcPort = 8545, useHttps = false) {
        this._rpcAddress = rpcAddress;
        this._rpcPort = rpcPort;
        this._protocol = useHttps ? 'https://' : 'http://';
    }

    public call(command: string, params = [], id = 1): Promise<any> {
        return new Promise((resolve, reject) => {
            let client = new Client();
            let rpcCommand = {
                method: command,
                params: params,
                id: id,
                jsonrpc: "2.0"
            };

            const args = {data: rpcCommand, headers: {"Content-Type": "application/json"}};

            client.post(`${this._protocol}${this._rpcAddress}:${this._rpcPort}`,
                args, (data, response) => {
                    if (response.statusCode == 200) {
                        resolve(data.result);
                    } else {
                        reject(new Error(response.statusMessage));
                    }
                });
        });
    };
}