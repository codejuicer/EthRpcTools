let Client = require('node-rest-client').Client;

export type RestMethod = "POST" | "GET" | "DELETE" | "PUT";

export interface EndpointParameter {
  name: string,
  type: string,
  min?: number,
  max?: number,
  validRange?: any[]
  default?: any,
  optional: boolean
}

export interface PathSubstitution {
  name: string,
  type: string
}

export interface EndpointDefinition {
  name: string,
  endpoint: string,
  restMethod: RestMethod,
  parameters?: EndpointParameter[],
  pathSubstitutions?: PathSubstitution[]
}

export interface IRestCall {
  url: string,
  restMethod: RestMethod,
  parameters?: any,
  pathSubstitutions?: any,
  data?: any
}

/**
 * Executes a rest call.
 * @param {IRestCall} restArgs Rest call arguments.
 * @param data Optional data.
 * @returns {Promise<any>} The response.
 */
export function restCall(restArgs: IRestCall, data?: any): Promise<any> {

  let args = {};
  if(restArgs.parameters) args['parameters'] = restArgs.parameters;
  if(restArgs.pathSubstitutions) args['path'] = restArgs.pathSubstitutions;
  if(data) args['data'] = data;

  let req;
  let client = new Client();

  return new Promise((resolve, reject) => {
    switch(restArgs.restMethod) {
      case "GET":
        console.log('Get call.');
        req = client.get(restArgs.url, args, (data) => {
          resolve(data);
        });
        break;

      case "POST":
        req = client.post(restArgs.url, args, (data) => {
          resolve(data);
        });
        break;

      default:
        reject(new Error(`Method ${restArgs.restMethod} is not supported.`));
    }

    req.on('error', (err) => {
      reject(err);
    });
  });
}