import {EndpointDefinition} from "../net";
import {PathSubstitution} from "../net/Rest";

export interface ICurrencyListing {
  id: number,
  name: string,
  symbol: string
}

export interface ICurrencyInfo {
  price: number,
  volume?: number,
  marketCap?: number,
  percentChange1Hour?: number,
  percentChange1Day?: number,
  percentChange1Week?: number,
  percentChange1Month?: number,
  percentChange1Year?: number
}

export interface ApiParameter {
  key: string,
  value: any
}

export interface IMarketApi {
  apiUrl: string,
  endpoints: string[],
  getEndpointDefinition(endpointName: string): EndpointDefinition,
  hasEndpoint(endpointName: string): boolean,
  callIsValid(endpointName: string, parameters: any): boolean
}

/**
 * Builder interface for an API call
 */
export interface IApiCall {
  call(): IApiCall,
  asPost(): IApiCall,
  asGet(): IApiCall,
  withUrl(url: string): IApiCall,
  withEndpoint(endpoint: EndpointDefinition): IApiCall,
  withParameter(parameter: ApiParameter): IApiCall,
  withParameters(parameters: ApiParameter[]): IApiCall,
  withPathSubstitution(pathSubstitution: ApiParameter): IApiCall,
  withPathSubstitutions(pathSubstitutions: ApiParameter[]): IApiCall,
  validateCall(): IApiCall,
  invoke(): Promise<any>
}