import {ApiParameter, IApiCall, IMarketApi} from "./IMarketApi";
import {EndpointDefinition, PathSubstitution, restCall, RestMethod} from "../net/Rest";

export class MarketApi implements IApiCall {
  private readonly _marketApi: IMarketApi;
  private _endpointDefinition: EndpointDefinition;
  private _parameters: any;
  private _callType: RestMethod;
  private _pathSubstitutions: any;

  constructor(marketApi: IMarketApi) {
    this._marketApi = marketApi;
  }

  /**
   * Start a new API call. Clears previous values.
   * Sets the call type to GET by default.
   * @returns {IMarketApi}
   */
  call(): IApiCall {
    this._endpointDefinition = undefined;
    this._parameters = {};
    this._callType = 'GET';
    return this;
  }

  async invoke(): Promise<any> {
    const apiEndpoint = [this._marketApi.apiUrl, this._endpointDefinition.endpoint].join('/');
    console.log(`Endpoint URL: ${apiEndpoint}`);

    return restCall({
        url: apiEndpoint,
        restMethod: this._callType,
        parameters: this._parameters,
        pathSubstitutions: this._pathSubstitutions
      });
  }

  asPost(): IApiCall {
    this._callType = 'POST';
    return this;
  }

  asGet(): IApiCall {
    this._callType = 'GET';
    return this;
  }

  /**
   * Sets the URL for the API.
   * @param {string} url API URL.
   * @returns {IMarketApi} Instance of the API object.
   */
  withUrl(url: string): IApiCall {
    this._marketApi.apiUrl = url;
    return this;
  }

  /**
   * Sets the API method endpoint, along with the rest method.
   * @param {string} endpoint Method endpoint.
   * @returns {IMarketApi} Instance of the API object.
   */
  withEndpoint(endpoint: EndpointDefinition): IApiCall {
    console.log(`Using endpoint: ${endpoint.name}`);
    this._endpointDefinition = endpoint;
    this._callType = endpoint.restMethod;
    return this;
  }

  /**
   * Adds a single parameter.
   * @param {ApiParameter} parameter The parameter to add.
   * @returns {IApiCall} Instance of this object for chaining.
   */
  withParameter(parameter: ApiParameter): IApiCall {
    this._parameters[parameter.key] = parameter.value;
    return this;
  }

  /**
   * Adds multiple parameters.
   * @param {[ApiParameter]} parameters An array of parameters.
   * @returns {IApiCall} Instance of this object for chaining.
   */
  withParameters(parameters: [ApiParameter]): IApiCall {
    for(let parameter of parameters) {
      this._parameters[parameter.key] = parameter.value;
    }
    return this;
  }

  withPathSubstitution(pathSubstitution: ApiParameter): IApiCall {
    this._pathSubstitutions[pathSubstitution.key] = pathSubstitution.value;
    return this;
  }

  withPathSubstitutions(pathSubstitutions: ApiParameter[]): IApiCall {
    for(let pathSub of pathSubstitutions) {
      this._pathSubstitutions[pathSub.key] = pathSub.value;
    }
    return this;
  }

  validateCall(): IApiCall {
    throw new Error('Validation not implemented');
    // return this;
  }

}