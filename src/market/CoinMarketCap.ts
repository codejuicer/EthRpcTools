import {IMarketApi} from "./IMarketApi";
import {EndpointDefinition} from "../net";

/**
 * Class to interact with the Coin Market Cap API.
 */
export class CoinMarketCap implements IMarketApi {
  private _apiUrl: string;

  constructor(apiUrl?: string) {
    this._apiUrl = apiUrl || 'https://api.coinmarketcap.com/v2';
  }

  /**
   * Getter for api URL.
   * @returns {string}
   */
  get apiUrl(): string {
    return this._apiUrl;
  }

  /**
   * Setter for the api URL.
   * @param {string} url
   */
  set apiUrl(url: string) {
    this._apiUrl = url;
  }

  /**
   * Gets all the endpoint names.
   * @returns {string[]} Array of endpoint names.
   */
  get endpoints(): string[] {
    return CoinMarketCap._endpoints.map((endpointDefinition) => {
      return endpointDefinition.name;
    });
  }

  /**
   * Validates a call based on the endpoint definition.
   * @param {string} endpointName Endpoint name.
   * @param parameters Endpoint parameters.
   * @returns {boolean} Validity of the call.
   */
  callIsValid(endpointName: string, parameters: any): boolean {
    throw new Error('Not implemented.');
  }

  /**
   * Fetches an endpoint definition by endpoint name.
   * @param {string} endpointName Name of the endpoint.
   * @returns {EndpointDefinition} The definition.
   */
  getEndpointDefinition(endpointName: string): EndpointDefinition {
    return CoinMarketCap._endpoints.find((endpointDefinition) => {
      return endpointDefinition.name == endpointName;
    });
  }

  /**
   * Checks for the existence of an endpoint.
   * @param {string} endpoint The endpoint to search for.
   * @returns {boolean} Whether or not the endpoint is valid.
   */
  hasEndpoint(endpointName: string): boolean {
    let match = CoinMarketCap._endpoints.find((endpointDefinition) => {
      return endpointDefinition.name == endpointName;
    });

    return !!match;
  }

  /**
   * Supported endpoints for the service.
   * @private
   */
  private static readonly _endpoints: EndpointDefinition[] = [
    {
      name: 'listings',
      endpoint: 'listings/',
      restMethod: 'GET'
    },
    {
      name: 'ticker',
      endpoint: 'ticker/',
      restMethod: 'GET',
      parameters: [
        {
          name: 'start',
          type: 'int',
          optional: true,
          default: 1
        },
        {
          name: 'limit',
          type: 'int',
          default: 100,
          max: 100,
          optional: true
        },
        {
          name: 'convert',
          type: 'string',
          optional: true,
          validRange: ["AUD", "BRL", "CAD", "CHF", "CLP", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "IDR", "ILS",
            "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PKR", "PLN", "RUB", "SEK", "SGD", "THB", "TRY",
            "TWD", "ZAR", "BTC", "ETH", "XRP", "LTC", "BCH"]
        }
      ]
    },
    {
      name: 'specific ticker',
      endpoint: 'ticker/${id}/',
      restMethod: 'GET',
      parameters: [
        {
          name: 'convert',
          type: 'string',
          optional: true,
          validRange: ["AUD", "BRL", "CAD", "CHF", "CLP", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "IDR", "ILS",
            "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PKR", "PLN", "RUB", "SEK", "SGD", "THB", "TRY",
            "TWD", "ZAR", "BTC", "ETH", "XRP", "LTC", "BCH"]
        }
      ],
      pathSubstitutions: [
        {
          name: 'id',
          type: 'number'
        }
      ]
    },
    {
      name: 'global',
      endpoint: 'global/',
      restMethod: 'GET',
      parameters: [
        {
          name: 'convert',
          type: 'string',
          optional: true,
          validRange: ["AUD", "BRL", "CAD", "CHF", "CLP", "CNY", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "IDR", "ILS",
            "INR", "JPY", "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PKR", "PLN", "RUB", "SEK", "SGD", "THB", "TRY",
            "TWD", "ZAR", "BTC", "ETH", "XRP", "LTC", "BCH"]
        }
      ]
    }
  ];
}