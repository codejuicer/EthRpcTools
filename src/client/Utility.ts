/**
 * Standard console colors.
 */
export enum ConsoleColors {
    BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE
}

/**
 * Standard console text formatting.
 */
export enum ConsoleTextFormat {
    RESET, BOLD, UNDERLINE = 4, INVERSE = 7
}

export interface ConsoleColorOptions {
    bold?: boolean,
    underline?: boolean,
    inverse?: boolean,
    reset?: boolean,
    fg?: ConsoleColors,
    bg?: ConsoleColors
}

export class StringUtility {

    private constructor() {
    }

    /**
     * Takes an extraData base-64 value and converts it back to ASCII.
     * @param {string} extraData
     * @returns {string}
     */
    public static parseExtra(extraData: string) {
        let extraString = '';
        for (let i = 2, end = extraData.length; i < end; i += 2) {
            extraString += String.fromCharCode(parseInt(extraData.substr(i, 2), 16));
        }
        return extraString;
    }

    public static colorString(font, text): string {
        let colorCommands = [];

        if(font['bold'] === true) { colorCommands.push(ConsoleTextFormat.BOLD); }
        if(font['fg'] !== undefined) { colorCommands.push('3' + font['fg']); }
        if(font['bg'] !== undefined) { colorCommands.push('4' + font['bg']); }
        if(font['underline'] === true) { colorCommands.push(ConsoleTextFormat.UNDERLINE); }
        if(font['inverse'] === true) { colorCommands.push(ConsoleTextFormat.INVERSE); }
        if(font['reset'] === true) { colorCommands.push(ConsoleTextFormat.RESET);}

        return ('\u001b[' + colorCommands.join(';') + 'm' + text + '\u001b[0m');
    }

}