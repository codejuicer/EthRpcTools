export interface ITransaction {
    "hash": string,
    "nonce": string,
    "blockHash": string,
    "blockNumber": number,
    "transactionIndex": number,
    "from": string,
    "to": string,
    "value": number,
    "gas": number,
    "gasPrice": number,
    "input": string
}

export interface IBlock {
    "number": number,
    "hash": string,
    "parentHash": string,
    "nonce": string,
    "sha3Uncles": string,
    "logsBloom": string,
    "transactionsRoot": string,
    "stateRoot": string,
    "miner": string,
    "difficulty": number,
    "totalDifficulty": number,
    "extraData": string,
    "size": number,
    "gasLimit": number,
    "gasUsed": number,
    "timestamp": number,
    "transactions": ITransaction[]
    "uncles": string[]
}