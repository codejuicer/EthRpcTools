import {RpcClient, RpcMethods} from "../net/JsonRpc";
import {Block, NephewBlocks} from "./Block";
import {StringUtility} from "./Utility";

export interface blockSearchParams {
    account?: string,
    startBlock?: number,
    endBlock?: number
}

/**
 * Account information and helpful functions.
 */
export class Account {
    private readonly _rpcClient: RpcClient;
    private _account: string;

    constructor(accountHash: string, rpcClient?: RpcClient) {
        // Use the given RpcClient, or construct a default one.
        this._rpcClient = rpcClient || (new RpcClient());
        this._account = accountHash;
    }

    /**
     * Returns the account number being worked with.
     * @returns {string}
     */
    get account() {
        return this._account;
    }

    /**
     * Sets the account number to work with.
     * @param account
     */
    set account(account) {
        this._account = account;
    }

    /**
     * Gets the balance of the account based on the 'latest' default block.
     * @returns {Promise<number>} Account balance, or undefined on promise rejection.
     */
    get balance(): Promise<number> {
        return new Promise<number>((resolve => {
            this._rpcClient.call(RpcMethods.eth.getBalance, [this._account, 'latest'])
                .then(balance => {
                    resolve(Number.parseInt(balance));
                })
                .catch(() => {
                    resolve(undefined);
                });
        }));
    }

    /**
     * Searches for blocks mined by the given account.
     * Search parameters is a JSON object that contain any of the following optional strings:
     * - startBlock: Block number to start searching at - inclusive. Defaults to endBlock - 1000.
     * - endBlock: Block number to end searching at - inclusive. Defaults to current block.
     * @returns {Promise<any>}
     * @param searchParameters
     * @param verbose Logs some messages during the process. Default = true.
     */
     public async getMinedBlocks(searchParameters: blockSearchParams = {}, verbose = true): Promise<any> {
        // Defaults to current block.
        const endBlock = searchParameters.hasOwnProperty('endBlock') ? Math.max(0,searchParameters.endBlock) :
            await Block.currentBlock(this._rpcClient);
        // Defaults to current block - 1000
        const startBlock = searchParameters.hasOwnProperty('startBlock') ? Math.max(0, Math.min(searchParameters.startBlock, endBlock)) :
            Math.max(endBlock - 1000, 0);

        let blocksFound: Block[] = [];

        console.log(`Starting mined block search. Blocks ${startBlock} - ${endBlock}`);
        console.log(`Account: ${this.account}`);

        // Iterate over blocks to find matches between "miner" and the account number.
        try {
            for (let blockNumber = startBlock; blockNumber <= endBlock; blockNumber++) {
                // Log for semi-long processes if desired.
                if(verbose && blockNumber%500 == 0) { console.log(`Searching block ${blockNumber}`); }

                let currentBlock: Block = await Block.getByNumber(this._rpcClient, blockNumber);
                // Bug out!
                if(!currentBlock) {
                    console.log(`No block at number! ${blockNumber}`);
                    break;
                }

                if(currentBlock.miner === this.account) {
                    blocksFound.push(currentBlock);
                }
            }
            return Promise.resolve(blocksFound);
        } catch (err) {
            return Promise.reject(err);
        }
    };
}