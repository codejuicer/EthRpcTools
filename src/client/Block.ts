import {RpcClient} from "../net/JsonRpc";
import {IBlock, ITransaction} from "./ResultInterfaces";
let eth = require('../net/JsonRpc').RpcMethods.eth;

export interface NephewBlocks {
    blockNumber: number,
    nephews: [string]
}

/**
 * A helper class for getting information about blocks.
 */
export class Block implements IBlock {
    private readonly _number: number;
    private readonly _difficulty: number;
    private readonly _extraData: string;
    private readonly _gasLimit: number;
    private readonly _gasUsed: number;
    private readonly _hash: string;
    private readonly _logsBloom: string;
    private readonly _miner: string;
    private readonly _nonce: string;
    private readonly _parentHash: string;
    private readonly _receiptsRoot: string;
    private readonly _sha3Uncles: string;
    private readonly _size: number;
    private readonly _stateRoot: string;
    private readonly _timestamp: number;
    private readonly _totalDifficulty: number;
    private readonly _transactions: any[];
    private readonly _transactionsRoot: string;
    private readonly _uncles: string[];

    /**
     * Block number
     * @returns {number}
     */
    get number(): number {
        return this._number;
    }

    /**
     * Block difficulty
     * @returns {number}
     */
    get difficulty(): number {
        return this._difficulty;
    }

    /**
     * Extra data from miner
     * @returns {string}
     */
    get extraData(): string {
        return this._extraData;
    }

    /**
     * Gas limit
     * @returns {number}
     */
    get gasLimit(): number {
        return this._gasLimit;
    }

    /**
     * Actual gas used
     * @returns {number}
     */
    get gasUsed(): number {
        return this._gasUsed;
    }

    /**
     * Block hash
     * @returns {string}
     */
    get hash(): string {
        return this._hash;
    }

    /**
     * Logs bloom... whatever that means
     * @returns {string}
     */
    get logsBloom(): string {
        return this._logsBloom;
    }

    /**
     * Hash of miner that found the block
     * @returns {string}
     */
    get miner(): string {
        return this._miner;
    }

    /**
     * Block nonce
     * @returns {string}
     */
    get nonce(): string {
        return this._nonce;
    }

    /**
     * Parent hash
     * @returns {string}
     */
    get parentHash(): string {
        return this._parentHash;
    }

    /**
     * Receipt root.
     * @returns {string}
     */
    get receiptsRoot(): string {
        return this._receiptsRoot;
    }

    /**
     * SHA-3 of the uncles
     * @returns {string}
     */
    get sha3Uncles(): string {
        return this._sha3Uncles;
    }

    /**
     * Block size
     * @returns {number}
     */
    get size(): number {
        return this._size;
    }

    /**
     * State root. Don't know what that is.
     * @returns {string}
     */
    get stateRoot(): string {
        return this._stateRoot;
    }

    /**
     * Unix timestamp block was found
     * @returns {number}
     */
    get timestamp(): number {
        return this._timestamp;
    }

    /**
     * Total difficulty.
     * @returns {number}
     */
    get totalDifficulty(): number {
        return this._totalDifficulty;
    }

    /**
     * Transactions contained in the block.
     * @returns {any[]}
     */
    get transactions(): ITransaction[] {
        return this._transactions;
    }

    /**
     * Transactions root. Another mystery to me.
     * @returns {string}
     */
    get transactionsRoot(): string {
        return this._transactionsRoot;
    }

    /**
     * Uncles of this block.
     * @returns {string[]}
     */
    get uncles(): string[] {
        return this._uncles;
    }

    /**
     * Builds a new Block object from JSON block data.
     * @param blockData
     */
    constructor(blockData: any) {
        this._number = Number(blockData["number"]);
        this._difficulty = Number(blockData.difficulty);
        this._extraData = blockData.extraData;
        this._gasLimit = Number(blockData.gasLimit);
        this._gasUsed = Number(blockData.gasUsed);
        this._hash = blockData.hash;
        this._logsBloom = blockData.logsBloom;
        this._miner = blockData.miner;
        this._nonce = blockData.nonce;
        this._parentHash = blockData.parentHash;
        this._receiptsRoot = blockData.receiptsRoot;
        this._sha3Uncles = blockData.sha3Uncles;
        this._size = Number(blockData.size);
        this._stateRoot = blockData.stateRoot;
        this._timestamp = Number(blockData.timestamp);
        this._totalDifficulty = Number(blockData.totalDifficulty);
        this._transactions = blockData.transactions;
        this._transactionsRoot = blockData.transactionsRoot;
        this._uncles = blockData.uncles;
    }

    /**
     * Gets the top block.
     * @param {RpcClient} rpcClient
     * @returns {Promise<number>}
     */
    public static async currentBlock(rpcClient: RpcClient): Promise<number> {
        return await rpcClient.call(eth.blockNumber);
    }

    /**
     * Fetches a block from the client and constructs a new Block object.
     * @param {RpcClient} rpcClient Client to get the block from.
     * @param {number} blockNumber Block number to fetch.
     * @returns {Promise<Block>} The block, or undefined if not found.
     */
    public static async getByNumber(rpcClient: RpcClient, blockNumber: number): Promise<Block> {
        try {
            let block = await rpcClient.call(eth.getBlockByNumber,
                [`0x${blockNumber.toString(16)}`, true]);

            return Promise.resolve(new Block(block));
        } catch(fetchError) {
            return Promise.resolve(undefined);
        }
    }

}