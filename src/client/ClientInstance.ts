import {RpcClient} from "../net";
import {Account} from "./Account";

let ethMethods = require('../net/JsonRpc').RpcMethods.eth;
let personalMethods = require('../net/JsonRpc').RpcMethods.personal;

export interface Coin {
  coinName: string
  coinAbbreviation: string
}

/**
 * Helper class for interacting with a geth compatible client.
 */
export class ClientInstance {
  private readonly _rpcRunner: RpcClient;
  private _coin: Coin;

  constructor(rpcClient?: RpcClient, coin?: Coin) {
    // Use the given RpcClient, or construct a default one.
    this._rpcRunner = rpcClient || (new RpcClient());
    this._coin = coin || {coinName: 'Ethereum', coinAbbreviation: 'ETH'};
  }

  /**
   * Accessor to get the coin name.
   * @returns {string} Coin name.
   */
  get coinName() {
    return this._coin.coinName;
  }

  /**
   * Accessor to get the coin abbreviation.
   * @returns {string}
   */
  get coinAbbreviation() {
    return this._coin.coinAbbreviation;
  }

  /**
   * Accessor for changing the coin type.
   * @param coin Coin details. Must match {name: string, abbreviation: string}
   */
  set coin(coin: Coin) {
    this._coin = coin;
  }

  /**
   * Get's the server's coin type.
   * @returns {Coin}
   */
  get coin(): Coin {
    return this._coin;
  }

  /**
   * Accessor to get the client's coinbase.
   * @returns {Promise<string>}
   */
  get coinbase(): Promise<string> {
    return this.getCoinbase();
  }

  /**
   * Accessor to get all the client's accounts.
   * @returns {Promise<string[]>}
   */
  get accounts(): Promise<string[]> {
    return this.getAccountList();
  }

  get rpcClient(): RpcClient {
    return this._rpcRunner;
  }

  /**
   * Fetches the coinbase account number for the server being polled. If it can't, it will return 0x0.
   * @returns {Promise<string>} Coinbase account number.
   */
  private async getCoinbase(): Promise<string> {
    try {
      let coinbase = await this._rpcRunner.call(ethMethods.coinbase);
      return Promise.resolve(coinbase);
    } catch(err) {
      return Promise.resolve(undefined);
    }
  }

  /**
   * Gets the list of accounts for the client.
   * @returns {Promise<string[]>}
   */
  private async getAccountList() : Promise<string[]> {
    try {
      let accounts = await this._rpcRunner.call(ethMethods.accounts);
      return Promise.resolve(accounts);
    } catch(err) {
      return Promise.resolve(undefined);
    }
  }

  /**
   * Retrieves an account based on the account hash.
   * @param {string} accountHash The account hash.
   * @returns {Account} Account object.
   */
  public getAccount(accountHash: string): Account {
    return new Account(accountHash, this._rpcRunner);
  }

  /**
   * Creates a new account with the given password.
   * @param {string} password Password for the account.
   * @returns {Promise<Account>} Account object.
   */
  public async createAccount(password: string): Promise<Account> {
    // Create a new account.
    let accountHash = await this._rpcRunner.call(personalMethods.newAccount, [password]);
    // Clear for security.
    password = undefined;

    return new Account(accountHash, this._rpcRunner);
  }

}
